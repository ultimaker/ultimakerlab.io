# Pipeline overview

This repository holds Ultimaker's public pipeline overview.

Reinstallation:

apt-get install openssh-server unclutter chromium-browser xorg
timedatectl set-timezone Europe/Amsterdam

cp /etc/X11/Xsession .xinitrc

place:

```sh

exec "/home/piper/pipeshow.sh"

```

before 'exit 0' at the end of the file.
